/*
Создать  собственное исключение и вызвать его в коде.
 */

public class ExceptionsExTwo {
    public static void main(String[] args) {
        int[] array = {1, 2, 3};
        try {
            sum(array);
        } catch (InvalidInputParamExeption e) {
            System.out.println("Отловили и обработали ошибку, появившуюся в методе sum");
            e.printStackTrace();
        }
    }

    private static void sum(int[] array) {
        try {
            int sum = array[5]+array[10];
            System.out.println(sum);
        } catch (IndexOutOfBoundsException e) {
            throw new InvalidInputParamExeption("Указанные элементы отсутствуют в переданном массиве. Ошибка в методе sum");
        }
    }

    public static class InvalidInputParamExeption extends RuntimeException {
        public InvalidInputParamExeption(String message) {
            super(message);
        }
    }
}
/*
Машина имеет двигатель, колеса , марку и скорость может ехать, тормозить и приветствовать хозяина .
Двигатель машины  имеет мощность, поршины и вес может заводиться, работать и издавать звук. Колеса
имеют диаметр, тип и фирму изготовителя могут изнашиваться и оставлять след.
 */

public class Car {
    String brand;
    int speed;
    Engine engine;
    Wheels[] wheels;


    Car(Engine engine, Wheels[] wheels, String brand, int speed) {
        this.engine = engine;
        this.wheels = wheels;
        this.brand = brand;
        this.speed = speed;
    }

    public void drive() {
        System.out.println("Поехали");
    }
    public void stop() {
        System.out.println("Стоп");
    }
    public void sayHello() {
        System.out.println("Привет, хозяин");
    }
}

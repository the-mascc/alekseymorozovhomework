/*
 Дано вещественное число — цена 1 кг конфет. Вывести стоимость 2,4, …, 10 кг конфет.
 */

public class LessonTwoExOne {
    public static void main(String[] args) {
        float pricePerKG = 5.9F;
        float price;
        System.out.println("Стоимость 1кг конфет = " + pricePerKG + "рублей");
        for (int i = 2; i <= 10; i += 2) {
            price = i * pricePerKG;
            System.out.println("Стоимость " + i + "кг конфет будет равняться " + price + " рублей");
        }
    }
}

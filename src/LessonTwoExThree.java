/*
Найти среднее арифметическое элементов массива
 */

public class LessonTwoExThree {
    public static void main(String[] args) {
        int[] array = { 15, 20, 25, 30 };
        float arraySum = 0;
        for (int j : array) {
            arraySum += j;
        }
        System.out.println("Сумма элементов массива = " + arraySum);
        float avgSum = arraySum/array.length;
        System.out.println("Среднее арифметическое элементов массива = " + avgSum);
    }
}

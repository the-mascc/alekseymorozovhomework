/*
В коллекции из 30 элементов числа образуют неубывающую последовательность. Найти количество различных чисел в коллекции.
 */

import java.util.*;

public class CollectionsExThree {
    public static void main(String[] args) {
        List<Integer> list1 = new ArrayList<>(30);
        for (int i = 0; i < 30; i++) {
            list1.add(getRandomNumber());
        }
        Collections.sort(list1);
        int uniqueElements = findUniqueElements(list1);
        int repetitiveElements = list1.size() - uniqueElements;
        System.out.println("Отсортированная коллеция: " + list1);
        System.out.println("Колличество уникальных чисел в коллекции = " + uniqueElements);
        System.out.println("Количество повторяющихся чисел: " + repetitiveElements);
    }

    private static int getRandomNumber() {
        return (int)(Math.random() * 100);
    }

    public static int findUniqueElements(List<Integer> list) {
        Set<Integer> newList = new HashSet<>(list);
        return newList.size();
    }
}
/*
Определить, имеются ли в коллекции одинаковые элементы.
 */


import java.util.*;

public class CollectionsExTwo {
    public static void main(String[] args) {
        Integer[] array = {1, 2, 7, 6, 2, 1, 3, 2};
        String[] array2 = {"one", "two", "three", "two", "four", "zero", "one"};
        String[] array3 = {"one", "two", "three", "four", "zero"};
        List<Integer> list = new ArrayList<>(Arrays.asList(array));
        List<String> list2 = new ArrayList<>(Arrays.asList(array2));
        List<String> list3 = new ArrayList<>(Arrays.asList(array3));
        findRepeat(list);
        findRepeat(list2);
        findRepeat(list3);
    }

    public static <T> void findRepeat(List<T> list) {
        Set<T> newList = new HashSet<>(list);
        if (list.size() > newList.size()) {
            System.out.println("В коллекции имеются одинаковые элементы");
        } else {
            System.out.println("В коллекции повторы отсутствуют");
        }
    }
}
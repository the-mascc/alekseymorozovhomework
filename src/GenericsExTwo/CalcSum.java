package GenericsExTwo;

class CalcSum<T extends Number> {
    public T sumValue(T firstNumber, T secondNumber, Adder<T> adder) {
        return adder.add(firstNumber, secondNumber);
    }
}
